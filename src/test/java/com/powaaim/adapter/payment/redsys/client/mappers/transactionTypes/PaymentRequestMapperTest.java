package com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import javax.ws.rs.core.MultivaluedMap;

import org.junit.Before;
import org.junit.Test;

import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.request.ProcessPaymentRequest;

public class PaymentRequestMapperTest {

    private PaymentRequestMapper requestMapper;
    private ProcessPaymentRequest processPaymentRequest;
    private String xml;

	
    @Before
    public void setUp() throws IOException {

        PaymentProvider paymentProvider = Fixtures.getPaymentProvider();
        processPaymentRequest = Fixtures.getPaymentRequest();
        requestMapper = new PaymentRequestMapper(paymentProvider);
        MultivaluedMap<String, String> formFields = requestMapper.mapRequestToFormFields(requestMapper.map(processPaymentRequest));
        xml = formFields.getFirst("entrada");
    }
	
	@Test
	public void test() {
		assertThat("amount value well converted",  xml, containsString("<DS_MERCHANT_AMOUNT>1234567</DS_MERCHANT_AMOUNT>"));
		assertThat("currency code well converted", xml, containsString("<DS_MERCHANT_CURRENCY>978</DS_MERCHANT_CURRENCY>"));
		assertThat("order number well converted",  xml, containsString("<DS_MERCHANT_ORDER>1234</DS_MERCHANT_ORDER>"));
		assertThat("merchant code well converted", xml, containsString("<DS_MERCHANT_MERCHANTCODE>327234688</DS_MERCHANT_MERCHANTCODE>"));
		assertThat("terminal code well converted", xml, containsString("<DS_MERCHANT_TERMINAL>2</DS_MERCHANT_TERMINAL>"));
		assertThat("transaction type = A",         xml, containsString("<DS_MERCHANT_TRANSACTIONTYPE>A</DS_MERCHANT_TRANSACTIONTYPE>"));
		assertThat("card number well formated",    xml, containsString("<DS_MERCHANT_PAN>4548812049400004</DS_MERCHANT_PAN>"));
		assertThat("expiry date well formated",    xml, containsString("<DS_MERCHANT_EXPIRYDATE>2012</DS_MERCHANT_EXPIRYDATE>"));
		assertThat("cvv2 well converted",          xml, containsString("<DS_MERCHANT_CVV2>123</DS_MERCHANT_CVV2>"));
		//TODO: check that not contains XML header not line breaks
//		assertThat("not contains XML header",      xml, not( containsString("xml") ));
		
	}

}

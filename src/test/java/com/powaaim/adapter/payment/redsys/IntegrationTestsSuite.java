package com.powaaim.adapter.payment.redsys;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.powaaim.adapter.payment.redsys.service.*;


@RunWith(Categories.class)
@Suite.SuiteClasses({
        PaymentTest.class,
        DeferredTest.class,
        CaptureTest.class,
        AbortTest.class,
        ConfigTest.class,
        RefundTest.class
})
public class IntegrationTestsSuite {}

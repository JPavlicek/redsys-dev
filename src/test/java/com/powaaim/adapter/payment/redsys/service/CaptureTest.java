package com.powaaim.adapter.payment.redsys.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;

import com.powaaim.common.model.bean.Money;
import com.powaaim.common.model.constant.PaymentCardType;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.adapter.payment.util.ClientHelper;
import com.powaaim.common.config.Context;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.constant.TransactionType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;
import com.powaaim.common.util.HttpClient;

public class CaptureTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(CaptureTest.class);

    private PaymentService paymentService;
    private PaymentProvider paymentProvider;

    @Before
    public void setUp() throws IOException {
        @SuppressWarnings("rawtypes")
		HttpClient client = ClientHelper.createRegularHttpClient();
        this.paymentProvider = Fixtures.getPaymentProvider();
        this.paymentService = new PaymentService();
        Context context = new Context();
        context.setHttpClient(client);
        this.paymentService.setContext(context);
    }


    @Test
    public void captureWithoutDeferred() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is ERROR SIS0059", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
    }

    @Test
    public void captureWithDeferred() throws IOException {

    	// DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


    	// CAPTURE after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


    	// CAPTURE after - 2nd time
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        // SIS0060 - Confirmation for this pre-authorization already exists
        assertThat("response status is ERROR - SIS0060", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));

    }

    @Test
    public void partialCaptureWithDeferred() throws IOException {

        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // CAPTURE after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(500));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is REJECTED ", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("SIS0062 - Amount to be confirmed exceeds limit", result.getPaymentResult().getMessage(), is(containsString("SIS0062")));
    }

    @Test
    public void captureExceedsDeferred() throws IOException {

        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // CAPTURE after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1500));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is REJECTED ", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("SIS0062 - Amount to be confirmed exceeds limit", result.getPaymentResult().getMessage(), is(containsString("SIS0062")));

    }

    @Test
    public void captureWithDifferentCard() throws IOException {

        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // CAPTURE after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        processPaymentRequest.getPaymentRequest().getPaymentCard().setType(PaymentCardType.BANKOFAMERICA);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

    }

    @Test
    public void captureWithDifferentCurrency() throws IOException {

        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // CAPTURE after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setCurrency(Currency.getInstance("USD"));
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is ERROR", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
        assertThat("SIS0027 - Error currency does not match that assigned for that Terminal.", result.getPaymentResult().getMessage(), is(containsString("SIS0027")));

    }
}

package com.powaaim.adapter.payment.redsys.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.math.BigDecimal;

import com.powaaim.common.exception.CustomException;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.adapter.payment.util.ClientHelper;
import com.powaaim.common.config.Context;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.constant.TransactionType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;
import com.powaaim.common.util.HttpClient;

public class RefundTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(RefundTest.class);

    private PaymentService paymentService;
    private PaymentProvider paymentProvider;

    @Before
    public void setUp() throws IOException {
        @SuppressWarnings("rawtypes")
		HttpClient client = ClientHelper.createRegularHttpClient();
        this.paymentProvider = Fixtures.getPaymentProvider();
        this.paymentService = new PaymentService();
        Context context = new Context();
        context.setHttpClient(client);
        this.paymentService.setContext(context);
    }


    @Test
    public void refundWithoutDeferred() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Merchant Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Merchant Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }

        // SIS0054 - No operation to make refund
        assertThat("response status is ERROR SIS0054", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("SIS0054 - No operation to make refund", result.getPaymentResult().getMessage(), is(containsString("SIS0054")));
    }

    @Test
    public void refundWithPayment() throws IOException {

    	// PAYMENT first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.PAYMENT);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


    	// REFUND after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


    	// REFUND after - 2nd time
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        // SIS0057 - Amount to be refunded exceeds limit
        assertThat("response status is ERROR - SIS0057", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("SIS0057 - Amount to be refunded exceeds limit", result.getPaymentResult().getMessage(), is(containsString("SIS0057")));
    }

    @Test
    public void refundWithCaptureAndDeferred() throws IOException {
        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // CAPTURE after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // REFUND after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(200));
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
    }

    @Test
    public void partialRefund() throws IOException {

        // PAYMENT first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.PAYMENT);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // partial REFUND
        processPaymentRequest = Fixtures.getPaymentRequest();
        //BigDecimal amount = processPaymentRequest.getPaymentRequest().getAmountTotal().getAmount().divide(new BigDecimal(2));
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(500));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // 2nd partial REFUND
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(200));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

    }

    @Test
    public void refundExceedsLimit() throws IOException {

        // PAYMENT first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.PAYMENT);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        // big REFUND
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1500));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("response status is ERROR - SIS0057", result.getPaymentResult().getMessage(), is(containsString("SIS0057")));

    }

    @Test
    public void refundWithDeferred() throws IOException {
        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // REFUND after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.REFUND);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("response status is ERROR - SIS0054", result.getPaymentResult().getMessage(), is(containsString("SIS0054")));

    }
}
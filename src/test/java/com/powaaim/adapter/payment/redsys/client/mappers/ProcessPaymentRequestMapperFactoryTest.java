package com.powaaim.adapter.payment.redsys.client.mappers;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.powaaim.adapter.payment.FunctionalityNotSupportedException;
import com.powaaim.adapter.payment.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionType;

public class ProcessPaymentRequestMapperFactoryTest {

	@Test
	public void testSupportedTypes() {
		PaymentProvider paymentProvider = Fixtures.getPaymentProvider();
		ProcessPaymentRequestMapperFactory f = new ProcessPaymentRequestMapperFactory();
		ProcessPaymentRequestMapper<RedSysRequest> mapper;
		
		mapper = f.getMapper(TransactionType.PAYMENT, paymentProvider);
		assertThat("PAYMENT transaction mapped by PaymentRequestMapper", mapper, instanceOf(com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.PaymentRequestMapper.class));
		
		mapper = f.getMapper(TransactionType.DEFERRED, paymentProvider);
		assertThat("DEFERRED transaction mapped by DeferredRequestMapper", mapper, instanceOf(com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.DeferredRequestMapper.class));
		
		mapper = f.getMapper(TransactionType.CAPTURE, paymentProvider);
		assertThat("CAPTURE transaction mapped by CaptureRequestMapper", mapper, instanceOf(com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.CaptureRequestMapper.class));
		
		mapper = f.getMapper(TransactionType.ABORT, paymentProvider);
		assertThat("ABORT transaction mapped by AbortRequestMapper", mapper, instanceOf(com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.AbortRequestMapper.class));
		
		mapper = f.getMapper(TransactionType.VOID, paymentProvider);
		assertThat("VOID transaction mapped by RefundRequestMapper", mapper, instanceOf(com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.RefundRequestMapper.class));
		
		mapper = f.getMapper(TransactionType.REFUND, paymentProvider);
		assertThat("REFUND transaction mapped by RefundRequestMapper", mapper, instanceOf(com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.RefundRequestMapper.class));
		
		
		
	}
	
	@Test(expected=FunctionalityNotSupportedException.class)
	public void testUnsupportedTypes() {
		PaymentProvider paymentProvider = Fixtures.getPaymentProvider();
		ProcessPaymentRequestMapperFactory f = new ProcessPaymentRequestMapperFactory();
		ProcessPaymentRequestMapper<RedSysRequest> mapper;

		mapper = f.getMapper(TransactionType.AUTHENTICATE, paymentProvider);
		assertThat("AUTHENTICATE transaction mapped by PaymentRequestMapper", mapper, instanceOf(com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.DeferredRequestMapper.class));
	}	
	
}

package com.powaaim.adapter.payment.redsys.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.adapter.payment.util.ClientHelper;
import com.powaaim.common.config.Context;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.constant.TransactionType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;
import com.powaaim.common.util.HttpClient;

public class DeferredTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeferredTest.class);

    private PaymentService paymentService;
    private PaymentProvider paymentProvider;

    @Before
    public void setUp() throws IOException {
        HttpClient client = ClientHelper.createRegularHttpClient();
        this.paymentProvider = Fixtures.getPaymentProvider();
        this.paymentService = new PaymentService();
        Context context = new Context();
        context.setHttpClient(client);
        this.paymentService.setContext(context);
    }


    @Test
    public void shouldProcessDeferred() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
    }

    @Test
    public void deferredAfterPayment() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        String txCode = result.getPaymentResult().getProviderTxCode();
        processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        //SIS0051 - Repeated order number
        assertThat("response status is ERROR - SIS0051", result.getPaymentResult().getMessage(), is(containsString("SIS0051")));

    }


    @Test // is it needed if payment(above)=deferred+capture
    public void duplicateDeferred() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


        processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        //SIS0051 - Repeated order number
        assertThat("response status is ERROR - SIS0051", result.getPaymentResult().getMessage(), is(containsString("SIS0051")));

    }

}

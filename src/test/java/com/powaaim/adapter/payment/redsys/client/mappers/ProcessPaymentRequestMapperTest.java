package com.powaaim.adapter.payment.redsys.client.mappers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.powaaim.adapter.payment.FunctionalityNotSupportedException;
import com.powaaim.adapter.payment.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionType;
import com.powaaim.common.model.request.ProcessPaymentRequest;

public class ProcessPaymentRequestMapperTest {

	private RedSysRequest prepareRequest(TransactionType type) {
		try {
			ProcessPaymentRequestMapperFactory f = ProcessPaymentRequestMapperFactory.init();
			PaymentProvider paymentProvider = Fixtures.getPaymentProvider();
			ProcessPaymentRequestMapper<RedSysRequest> m = f.getMapper(type, paymentProvider);
			ProcessPaymentRequest processPaymentRequest = Fixtures.getPaymentRequest();
			
			return m.map(processPaymentRequest);
		} catch (IOException e) {
			fail("Cannot read data");
			return null;
		}
	}
	
	@Test
	public void checkPayment() {
		RedSysRequest redSysRequest = prepareRequest(TransactionType.PAYMENT);
	
		assertThat("bad transaction type", redSysRequest.getTransactionType(), is(equalTo("A")));
		assertNotNull("Credit card number missing", redSysRequest.getPan());
	}

	@Test
	public void checkDeferred() {
		RedSysRequest redSysRequest = prepareRequest(TransactionType.DEFERRED);
	
		assertThat("bad transaction type", redSysRequest.getTransactionType(), is(equalTo("O")));
		assertNotNull("Credit card number missing", redSysRequest.getPan());
	}

	@Test
	public void checkCapture() {
		RedSysRequest redSysRequest = prepareRequest(TransactionType.CAPTURE);
	
		assertThat("bad transaction type", redSysRequest.getTransactionType(), is(equalTo("P")));
		assertNull("Credit card number should be blank", redSysRequest.getPan());
	}

	@Test
	public void checkAbort() {
		RedSysRequest redSysRequest = prepareRequest(TransactionType.ABORT);
	
		assertThat("bad transaction type", redSysRequest.getTransactionType(), is(equalTo("Q")));
		assertNull("Credit card number should be blank", redSysRequest.getPan());
	}

	@Test
	public void checkRefund() {
		RedSysRequest redSysRequest = prepareRequest(TransactionType.REFUND);
	
		assertThat("bad transaction type", redSysRequest.getTransactionType(), is(equalTo("3")));
		assertNull("Credit card number should be blank", redSysRequest.getPan());
	}

	
//	@Test(expected=FunctionalityNotSupportedException.class)
	@Test
	public void checkVoid() {
		RedSysRequest redSysRequest = prepareRequest(TransactionType.VOID);
		
		assertThat("bad transaction type", redSysRequest.getTransactionType(), is(equalTo("3")));
		assertNull("Credit card number should be blank", redSysRequest.getPan());
	}
	
	
}

package com.powaaim.adapter.payment.redsys.client.mappers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import com.powaaim.adapter.payment.redsys.client.model.responses.RedSysResponse;
import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;

public class ProcessPaymentResponseFactoryTest {

	@Test
	public void paymentResponseOK() {
        ProcessPaymentRequest processPaymentRequest = null;
		try {
			processPaymentRequest = Fixtures.getPaymentRequest();
		} catch (IOException e) {
			fail("Cannot instantiate payment request");
		}
        RedSysResponse redSysResponse = Fixtures.loadResponse("UnitTests/RedSysResponse-payment.xml");
		com.powaaim.adapter.payment.client.mappers.ProcessPaymentResponseFactory<RedSysResponse> f = ProcessPaymentResponseFactory.init();
		ProcessPaymentResponse processPaymentResponse = f.create(processPaymentRequest, redSysResponse);

		assertThat("response status is OK", processPaymentResponse.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
		assertThat("authorization code", processPaymentResponse.getPaymentResult().getAuthorizationCode(), is(equalTo("426299")));
		assertThat("MerchantTxCode", processPaymentResponse.getPaymentResult().getMerchantTxCode(), is(equalTo("1234")));
		assertThat("ProviderTxCode", processPaymentResponse.getPaymentResult().getProviderTxCode(), is(equalTo("150824POWA01")));
		
		
	}
	
	@Test
	public void paymentResponseError() {
        ProcessPaymentRequest processPaymentRequest = null;
		try {
			processPaymentRequest = Fixtures.getPaymentRequest();
		} catch (IOException e) {
			fail("Cannot instantiate payment request");
		}
        RedSysResponse redSysResponse = Fixtures.loadResponse("UnitTests/RedSysResponse-error252.xml");
		com.powaaim.adapter.payment.client.mappers.ProcessPaymentResponseFactory<RedSysResponse> f = ProcessPaymentResponseFactory.init();
		ProcessPaymentResponse processPaymentResponse = f.create(processPaymentRequest, redSysResponse);

		assertThat("response status is ERROR", processPaymentResponse.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
		assertThat("response message contains error code", processPaymentResponse.getPaymentResult().getMessage(), containsString("SIS0252"));

	}
}

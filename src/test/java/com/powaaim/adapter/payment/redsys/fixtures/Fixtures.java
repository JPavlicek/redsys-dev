package com.powaaim.adapter.payment.redsys.fixtures;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import com.powaaim.adapter.payment.redsys.client.PaymentProviderClient;
import com.powaaim.adapter.payment.redsys.client.deserializers.RedSysResponseDeserializer;
import com.powaaim.adapter.payment.redsys.client.model.responses.RedSysResponse;
import com.powaaim.adapter.payment.redsys.client.model.responses.RedSysResponseTest;
import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.adapter.payment.redsys.service.PaymentService;
import com.powaaim.adapter.payment.util.ClientHelper;
import com.powaaim.adapter.payment.util.PowaJodaModule;
import com.powaaim.common.config.Context;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.bean.PaymentRequest;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.util.HttpClient;

public class Fixtures {

    private static Properties getTestProperties() throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream(Fixtures.class.getClassLoader().getResource("test.properties").getFile()));
        return props;

    }	
    
    /**
     * This endpoint has hard-coded values.
     *
     * @return
     * @throws IOException
     */
    public static PaymentProvider getPaymentProvider() {
        PaymentProvider paymentProvider = new PaymentProvider();
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("terminal", "2");
        paymentProvider.setMerchantId("327234688");
        paymentProvider.setMerchantPassword("qwertyasdf0123456789");
        paymentProvider.setUrl("https://sis-t.redsys.es:25443/sis/operaciones");
//        paymentProvider.setUrl("http://localhost/ejp/80-powa/10-redsys/response0.xml"); 
        paymentProvider.setProperties(properties);
        return paymentProvider;
    }
        
   

    public static ProcessPaymentRequest getPaymentRequest() throws IOException {
        return getProcessPaymentFromFile("UnitTests/PaymentRequest.json");
    }
	
    public static ProcessPaymentRequest getDeferredRequest() throws IOException {
        return getProcessPaymentFromFile("UnitTests/PaymentRequest.json");
    }
	    
	
    public static ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JaxbAnnotationModule());
        mapper.registerModule(new PowaJodaModule());
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }
	
    public static ProcessPaymentRequest getProcessPaymentFromFile(String fileName) throws IOException {
        ObjectMapper mapper = getObjectMapper();
        String orderDataFile = Fixtures.class.getClassLoader().getResource(fileName).getFile();
        ProcessPaymentRequest result = mapper.readValue(new File(orderDataFile), ProcessPaymentRequest.class);
        return result;
    }

	public static ProcessPaymentRequest getProcessPaymentWithUniqueId(ProcessPaymentRequest processPaymentRequest){
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {}
    	String tid = Long.valueOf(System.currentTimeMillis()/1000).toString() + "PW";
//    	LOGGER.info("MerchantTxCode:" + tid);
        processPaymentRequest.getPaymentRequest().setMerchantTxCode(tid);
        return processPaymentRequest;
    }
	
	public static PaymentProviderClient getClient() {
        HttpClient httpClient = ClientHelper.createRegularHttpClient();
        PaymentProvider paymentProvider = getPaymentProvider();        
		return new PaymentProviderClient(paymentProvider, httpClient);
	}
	
    public static InputStream getResponseInputStream() throws FileNotFoundException {
        return new FileInputStream(Fixtures.class.getClassLoader().getResource("UnitTests/RedSysResponse-payment.xml").getFile());
    }
	
    public static RedSysResponse loadResponse(String fname) {
		try {
	        String orderDataFile = RedSysResponseTest.class.getClassLoader().getResource(fname).getFile();
	        InputStream is;
			is = new FileInputStream(new File(orderDataFile));
	        
	    	return RedSysResponseDeserializer.deserialize(is);
		} catch (FileNotFoundException e) {
			fail("Response file not found");
		}
		return null;
		
	}
}

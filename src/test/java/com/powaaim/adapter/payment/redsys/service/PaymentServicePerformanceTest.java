package com.powaaim.adapter.payment.redsys.service;

import ch.qos.logback.classic.Level;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Stopwatch;
import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.common.config.Context;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.util.HttpClient;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * This is simple performance test for RedSys PaymentService without
 * communication with the gateway. HTTP communication (Jersey client) is mocked with Mockito.
 */
public class PaymentServicePerformanceTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentServicePerformanceTest.class);

    private static int TEST_COUNT = 500;

    private ProcessPaymentRequest payment;
    private PaymentProvider paymentProvider;
    private PaymentService paymentService;
    private Client client;


    @Before
    public void setUp() throws IOException {

        this.payment = Fixtures.getPaymentRequest();
        this.paymentProvider = Fixtures.getPaymentProvider();
        this.paymentService = new PaymentService();
        this.client = mock(Client.class);
        HttpClient httpClient = new HttpClient(client, new ObjectMapper());
        Context context = new Context();
        context.setHttpClient(httpClient);
        this.paymentService.setContext(context);

        WebResource resourceMock = mock(WebResource.class);
        WebResource.Builder builder = mock(WebResource.Builder.class);
        ClientResponse clientResponseMock = mock(ClientResponse.class);

        when(resourceMock.type((MediaType) anyObject())).thenReturn(builder);
        when(builder.accept((MediaType) anyObject())).thenReturn(builder);
        when(clientResponseMock.getEntity(InputStream.class)).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return Fixtures.getResponseInputStream();
            }
        });
        
        when(clientResponseMock.getType()).thenReturn(new MediaType("application", "x-www-form-urlencoded"));
        when(clientResponseMock.getStatus()).thenReturn(200);
        when(builder.post(any(Class.class), any(MultivaluedMap.class))).thenReturn(clientResponseMock);
        when(this.client.resource(anyString())).thenReturn(resourceMock);
    }

  
    
    @Test
    public void measureOverallPerformance() throws IOException {

    	ArrayList<Long> alTime = new ArrayList<>();  
//    	if (1==1) return;
        // Warm-up
        ch.qos.logback.classic.Logger rootLogger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        rootLogger.setLevel(Level.INFO);

        for(int i = 0; i < 10; i++) {
            this.paymentService.processPayment(this.paymentProvider, this.payment);
        }

        for(int i = 0; i < TEST_COUNT; i++) {
            Stopwatch stopwatch = new Stopwatch().start();
            this.paymentService.processPayment(this.paymentProvider, this.payment);
            long time = stopwatch.stop().elapsed(TimeUnit.MILLISECONDS);
            LOGGER.info("Test execution time: {} millis", time);
            alTime.add(time);
        }
        
        for (Long long1 : alTime) {
			System.out.print(long1 + " ");
		}
    }

}

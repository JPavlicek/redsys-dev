package com.powaaim.adapter.payment.redsys.client.model.requests;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class RedSysRequestTest {
	@Test
	public void hashWithCardNumber() {
		RedSysRequest r = new RedSysRequest();
	    r.setAmount("1000");
	    r.setOrder("150814POWA06");
		r.setMerchantCode("297395907");
		r.setCurrency("978");
		r.setPan("4548812049400004");
		r.setCvv2("123");
		r.setTransactionType("A");
		r.setTerminal("2");
		r.setExpiryDate("2012");
		r.computeHash("qwertyasdf0123456789");

		String xml = r.buildXML();
		assertThat("XML header",    	xml.toLowerCase(), not(containsString("<?xml")));
		assertThat("end of line \\n",   xml.toLowerCase(), not(containsString("\n")));
		assertThat("end of line \\r",   xml.toLowerCase(), not(containsString("\r")));

		assertThat("amount value",  	xml, containsString("<DS_MERCHANT_AMOUNT>1000</DS_MERCHANT_AMOUNT>"));
		assertThat("order number",  	xml, containsString("<DS_MERCHANT_ORDER>150814POWA06</DS_MERCHANT_ORDER>"));
		assertThat("merchant code",   	xml, containsString("<DS_MERCHANT_MERCHANTCODE>297395907</DS_MERCHANT_MERCHANTCODE>"));
		assertThat("currency code", 	xml, containsString("<DS_MERCHANT_CURRENCY>978</DS_MERCHANT_CURRENCY>"));
		assertThat("card number",    	xml, containsString("<DS_MERCHANT_PAN>4548812049400004</DS_MERCHANT_PAN>"));
		assertThat("cvv2",				xml, containsString("<DS_MERCHANT_CVV2>123</DS_MERCHANT_CVV2>"));
		assertThat("transaction type",	xml, containsString("<DS_MERCHANT_TRANSACTIONTYPE>A</DS_MERCHANT_TRANSACTIONTYPE>"));
		assertThat("terminal code",   	xml, containsString("<DS_MERCHANT_TERMINAL>2</DS_MERCHANT_TERMINAL>"));
		assertThat("expiry date",    	xml, containsString("<DS_MERCHANT_EXPIRYDATE>2012</DS_MERCHANT_EXPIRYDATE>"));
		
		assertThat("should return correct hash", r.getMerchantSignature(), is("56a91e68ee25d6f18fcaec1d0c32f247689823e3"));
	}

	@Test
	public void hashWithoutCardNumber() {
		RedSysRequest r = new RedSysRequest();
	    r.setAmount("145");
	    r.setOrder("150814POWA02");
		r.setMerchantCode("327234688");
		r.setCurrency("978");
		r.setTransactionType("P");
		r.setTerminal("2");
		r.computeHash("qwertyasdf0123456789");
		
		String xml = r.buildXML();
		assertThat("XML header",    	xml.toLowerCase(), not(containsString("<?xml")));
		assertThat("end of line \\n",   xml.toLowerCase(), not(containsString("\n")));
		assertThat("end of line \\r",   xml.toLowerCase(), not(containsString("\r")));

		assertThat("amount value",  	xml, containsString("<DS_MERCHANT_AMOUNT>145</DS_MERCHANT_AMOUNT>"));
		assertThat("order number",  	xml, containsString("<DS_MERCHANT_ORDER>150814POWA02</DS_MERCHANT_ORDER>"));
		assertThat("merchant code",   	xml, containsString("<DS_MERCHANT_MERCHANTCODE>327234688</DS_MERCHANT_MERCHANTCODE>"));
		assertThat("currency code", 	xml, containsString("<DS_MERCHANT_CURRENCY>978</DS_MERCHANT_CURRENCY>"));
		assertThat("card number",    	xml, not(containsString("<DS_MERCHANT_PAN>")));
		assertThat("cvv2",				xml, not(containsString("<DS_MERCHANT_CVV2>")));
		assertThat("transaction type",	xml, containsString("<DS_MERCHANT_TRANSACTIONTYPE>P</DS_MERCHANT_TRANSACTIONTYPE>"));
		assertThat("terminal code",   	xml, containsString("<DS_MERCHANT_TERMINAL>2</DS_MERCHANT_TERMINAL>"));
		assertThat("expiry date",    	xml, not(containsString("<DS_MERCHANT_EXPIRYDATE>")));
		
		assertThat("should return correct hash", r.getMerchantSignature(), is("dedf5c3c0eafcc0ad5fcef906c52cce55c292557"));
	}
}

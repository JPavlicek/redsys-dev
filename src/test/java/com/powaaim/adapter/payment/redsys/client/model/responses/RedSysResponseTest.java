package com.powaaim.adapter.payment.redsys.client.model.responses;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.powaaim.adapter.payment.redsys.client.PaymentProviderClient;
import com.powaaim.adapter.payment.redsys.client.deserializers.RedSysResponseDeserializer;
import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;

public class RedSysResponseTest {
/*
<?xml version='1.0' encoding="ISO-8859-1" ?>
<RETORNOXML>
  <CODIGO>SIS0252</CODIGO>
  <RECIBIDO>
    <DATOSENTRADA>
      <DS_MERCHANT_AMOUNT>1000</DS_MERCHANT_AMOUNT>
      <DS_MERCHANT_ORDER>150814POWA06</DS_MERCHANT_ORDER>
      <DS_MERCHANT_MERCHANTCODE>297395907</DS_MERCHANT_MERCHANTCODE>
      <DS_MERCHANT_CURRENCY>978</DS_MERCHANT_CURRENCY>
      <DS_MERCHANT_PAN>4548812049400004</DS_MERCHANT_PAN>
      <DS_MERCHANT_CVV2>123</DS_MERCHANT_CVV2>
      <DS_MERCHANT_TRANSACTIONTYPE>A</DS_MERCHANT_TRANSACTIONTYPE>
      <DS_MERCHANT_TERMINAL>2</DS_MERCHANT_TERMINAL>
      <DS_MERCHANT_EXPIRYDATE>2012</DS_MERCHANT_EXPIRYDATE>
      <DS_MERCHANT_MERCHANTSIGNATURE>56a91e68ee25d6f18fcaec1d0c32f247689823e3</DS_MERCHANT_MERCHANTSIGNATURE>
    </DATOSENTRADA>
  </RECIBIDO>
</RETORNOXML>
 */
	@Test
	public void paymentResponseError252() {		
		RedSysResponse response = loadResponse("UnitTests/RedSysResponse-error252.xml");
		assertThat("error code mapping", response.codigo, is("SIS0252"));
		assertThat("amount mapping",   			response.recibido.datosentrada.getAmount(), 			is("1000"));
		assertThat("order mapping", 			response.recibido.datosentrada.getOrder(), 				is("150814POWA06"));
		assertThat("merchant code mapping", 	response.recibido.datosentrada.getMerchantCode(), 		is("297395907"));
		assertThat("currecny mapping", 			response.recibido.datosentrada.getCurrency(), 			is("978"));
		assertThat("pan mapping", 				response.recibido.datosentrada.getPan(),				is("4548812049400004"));
		assertThat("cvv2 mapping", 				response.recibido.datosentrada.getCvv2(),	 			is("123"));
		assertThat("transaction type mapping", 	response.recibido.datosentrada.getTransactionType(), 	is("A"));
		assertThat("terminal mapping", 			response.recibido.datosentrada.getTerminal(), 			is("2"));
		assertThat("expity date mapping", 		response.recibido.datosentrada.getExpiryDate(), 		is("2012"));
		assertThat("signature mapping", 		response.recibido.datosentrada.getMerchantSignature(), 	is("56a91e68ee25d6f18fcaec1d0c32f247689823e3"));
	}

/*
<?xml version='1.0' encoding="ISO-8859-1" ?>
<RETORNOXML>
  <CODIGO>0</CODIGO>
  <OPERACION>
    <Ds_Amount>1000</Ds_Amount>
    <Ds_Currency>978</Ds_Currency>
    <Ds_Order>150824POWA01</Ds_Order>
    <Ds_Signature>08A63C62850FA8F63A7BAB278AFFACB05D0ECEE1</Ds_Signature>
    <Ds_MerchantCode>327234688</Ds_MerchantCode>
    <Ds_Terminal>2</Ds_Terminal>
    <Ds_Response>0000</Ds_Response>
    <Ds_AuthorisationCode>426299</Ds_AuthorisationCode>
    <Ds_TransactionType>A</Ds_TransactionType>
    <Ds_SecurePayment>0</Ds_SecurePayment>
    <Ds_Language>1</Ds_Language>
    <Ds_MerchantData></Ds_MerchantData>
    <Ds_Card_Country>724</Ds_Card_Country>
  </OPERACION>
</RETORNOXML>
*/	
	
	@Test
	public void paymentResponseOK() {
		RedSysResponse response = loadResponse("UnitTests/RedSysResponse-payment.xml");
		assertThat("error code mapping",		response.codigo, 						is("0"));
		assertThat("amount mapping",   			response.operacion.amount, 				is("1000"));
		assertThat("currecny mapping", 			response.operacion.currency, 			is("978"));
		assertThat("order mapping", 			response.operacion.order, 				is("150824POWA01"));
		assertThat("signature mapping", 		response.operacion.merchantSignature, 	is("08A63C62850FA8F63A7BAB278AFFACB05D0ECEE1"));
		assertThat("merchant code mapping", 	response.operacion.merchantCode, 		is("327234688"));
		assertThat("terminal mapping", 			response.operacion.terminal, 			is("2"));
		assertThat("response mapping", 			response.operacion.response, 			is("0000"));
		assertThat("authorisation code mapping",response.operacion.authorisationCode, 	is("426299"));
		assertThat("transaction type mapping", 	response.operacion.transactionType, 	is("A"));
		assertThat("secure payment mapping", 	response.operacion.securePayment, 		is("0"));
		assertThat("language mapping", 			response.operacion.language, 			is("1"));
		assertThat("merchant data mapping", 	response.operacion.merchantData, 		is("")); // change to something not empty
		assertThat("card country mapping", 		response.operacion.cardCountry, 		is("724"));
	}	
	
	
	@Test(expected=com.powaaim.common.exception.CustomException.class)
	public void paymentResponseBadFormat() {
		RedSysResponse response = loadResponse("UnitTests/RedSysResponse-badFormat.xml");
	}
	
	
	private RedSysResponse loadResponse(String fname) {
		try {
	        String orderDataFile = RedSysResponseTest.class.getClassLoader().getResource(fname).getFile();
	        InputStream is;
			is = new FileInputStream(new File(orderDataFile));
	        
	    	return RedSysResponseDeserializer.deserialize(is);
		} catch (FileNotFoundException e) {
			fail("Response file not found");
		}
		return null;
		
	}
	
}

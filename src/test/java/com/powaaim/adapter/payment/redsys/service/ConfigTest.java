package com.powaaim.adapter.payment.redsys.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;

import com.powaaim.common.model.constant.TransactionType;
import org.junit.Before;
import org.junit.Test;

import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.adapter.payment.util.ClientHelper;
import com.powaaim.common.config.Context;
import com.powaaim.common.exception.CustomException;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;
import com.powaaim.common.util.HttpClient;

public class ConfigTest {

    private PaymentService paymentService;
    private PaymentProvider paymentProvider;
	
    @Before
    public void setUp() throws IOException {
        HttpClient client = ClientHelper.createRegularHttpClient();
        this.paymentProvider = Fixtures.getPaymentProvider();
        this.paymentService = new PaymentService();
        Context context = new Context();
        context.setHttpClient(client);
        this.paymentService.setContext(context);
    }

	@Test(expected=CustomException.class)
	public void missingMerchantID() throws IOException {
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        this.paymentProvider.setMerchantId("");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
	}
    @Test(expected=CustomException.class)
    public void invalidMerchantID() throws IOException {
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        this.paymentProvider.setMerchantId("invalid`!@#!@id34985349534789"); // i don't know if this is invalid value
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
    }
    @Test
	public void missingMerchantPassword() throws IOException {
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        this.paymentProvider.setMerchantPassword("");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        assertThat("response status is ERROR", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
        assertThat("response status is SIS0042", result.getPaymentResult().getMessage(), containsString("SIS0042"));
	}

    @Test(expected=CustomException.class)
	public void missingTerminal() throws IOException {
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        this.paymentProvider.getProperties().remove("terminal");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
	}

    @Test(expected=CustomException.class)
	public void missingURL() throws IOException {
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        this.paymentProvider.setUrl("");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
	}

}

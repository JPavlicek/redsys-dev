package com.powaaim.adapter.payment.redsys.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;
import java.util.HashMap;

import com.powaaim.common.exception.CustomException;
import com.powaaim.common.model.bean.Address;
import com.powaaim.common.model.bean.Country;
import com.powaaim.common.model.constant.PaymentCardType;
import com.powaaim.common.model.constant.PaymentType;
import org.joda.time.YearMonth;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.adapter.payment.util.ClientHelper;
import com.powaaim.common.config.Context;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.bean.ServiceType;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.constant.TransactionType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;
import com.powaaim.common.util.HttpClient;

public class PaymentTest{

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentTest.class);

    private PaymentService paymentService;
    private PaymentProvider paymentProvider;


    @Before
    public void setUp() throws IOException {
        HttpClient client = ClientHelper.createRegularHttpClient();
        this.paymentProvider = Fixtures.getPaymentProvider();
        this.paymentService = new PaymentService();
        Context context = new Context();
        context.setHttpClient(client);
        this.paymentService.setContext(context);
    }


    @Test
    public void shouldProcessPayment() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
        LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());

        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
    }

    @Test
    public void duplicateProviderCode() throws IOException {
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String providerTxCode = processPaymentRequest.getPaymentRequest().getProviderTxCode();
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(providerTxCode);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("SIS0051 - Repeated order number", result.getPaymentResult().getMessage(), is(containsString("SIS0051")));
    }

    @Test
    public void badCreditCard() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getPaymentCard().setNumber("1111111111111111");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        assertThat("response status is ERROR", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
        // SIS0253 - Card does not comply with check-digit
        assertThat("SIS0253", result.getPaymentResult().getMessage(), is(containsString("SIS0253")));
    }

    @Test
    public void badCurrency() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getAmountTotal().setCurrency(Currency.getInstance("GBP"));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        // SIS0027 - Error currency does not match that assigned for that Terminal.
        assertThat("response status is ERROR", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
        assertThat("SIS0027", result.getPaymentResult().getMessage(), is(containsString("SIS0027")));
    }

    @Test
    public void badTerminal() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        PaymentProvider provider2 = Fixtures.getPaymentProvider();
        provider2.getProperties().put("terminal", "123");
        ProcessPaymentResponse result = this.paymentService.processPayment(provider2, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        // SIS0026 - Error Merchant inexistent / Terminal sent
        assertThat("response status is ERROR", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
        assertThat("SIS0026 - Error Merchant inexistent", result.getPaymentResult().getMessage(), is(containsString("SIS0026")));

    }

    @Test(expected = CustomException.class)
    public void badCVV() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getPaymentCard().setCvv("f2");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
    }

    @Test(expected = CustomException.class)
    public void negativeAmount() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(-100));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
    }

    @Test(expected = CustomException.class)
    public void blankCreditCardNR() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getPaymentCard().setNumber("");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
    }

    @Ignore // RedSys sandbox doesn't care about credit card validity and cvv2
    @Test(expected = CustomException.class)
    public void expiredCardDate() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getPaymentCard().setExpirationDate(YearMonth.fromDateFields(new Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000L)));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
    }

    @Test(expected=CustomException.class)
    public void notNumericCardNumber() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getPaymentCard().setNumber("not valid number & !@`'");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
    }

    @Test(expected=CustomException.class)
    public void longCardNumber() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getPaymentCard().setNumber("11112222333344445555");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
    }

    @Test
    public void blankEmailAddress() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getCustomer().setEmailAddress("");
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }

        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
    }

    @Test
    public void blankShippingAddress() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getCustomer().setShippingAddress(new Address("","","","","","","","","",new Country("", "")));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }

        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
    }

    @Test(expected=CustomException.class)
    public void payPalPaymentType() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setPaymentType(PaymentType.PAYPAL);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));

    }

    @Test
    public void duplicatePayment() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }
        assertThat("response status is REJECTED", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));
        assertThat("error code: SIS0051", result.getPaymentResult().getMessage(), is(containsString("SIS0051")));
    }


    @Test
    public void otherPaymentCardType() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().getPaymentCard().setType(PaymentCardType.BANKOFAMERICA);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        }

        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
    }

}

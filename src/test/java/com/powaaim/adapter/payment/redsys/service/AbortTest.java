package com.powaaim.adapter.payment.redsys.service;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.math.BigDecimal;

import com.powaaim.common.model.bean.Money;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.adapter.payment.redsys.fixtures.Fixtures;
import com.powaaim.adapter.payment.util.ClientHelper;
import com.powaaim.common.config.Context;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.constant.TransactionType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;
import com.powaaim.common.util.HttpClient;

public class AbortTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbortTest.class);

    private PaymentService paymentService;
    private PaymentProvider paymentProvider;

    @Before
    public void setUp() throws IOException {
        @SuppressWarnings("rawtypes")
		HttpClient client = ClientHelper.createRegularHttpClient();
        this.paymentProvider = Fixtures.getPaymentProvider();
        this.paymentService = new PaymentService();
        Context context = new Context();
        context.setHttpClient(client);
        this.paymentService.setContext(context);
    }


    @Test
    public void abortWithoutDeferred() throws IOException {

        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("expected response is ERROR SIS0223", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
        assertThat("expected response is ERROR SIS0223", result.getPaymentResult().getMessage(), is(containsString("SIS0223")));
    }

    @Test
    public void abortWithDeferred() throws IOException {

    	// DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


    	// ABORT after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));


    	// ABORT after - 2nd time
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        // SIS0060 - Confirmation for this pre-authorization already exists
        assertThat("expected response is ERROR - SIS0223", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));

    }

    @Test
    public void abortWithCaptureAndDeferred() throws IOException {
        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // CAPTURE after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.CAPTURE);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        assertThat("response status is SUCCESS", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // ABORT after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        // SIS0060 - Confirmation for this pre-authorisation already exists
        assertThat("response status is ERROR - SIS0060", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.REJECTED)));

    }

    @Test
    public void partialAbortWithDeferred() throws IOException {

        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // partial ABORT
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(100));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        assertThat("cannot abort payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // second partial ABORT
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(900));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        // SIS0223 - The pre-authorisation  to be cancelled is not authorised
        assertThat("expected response is ERROR - SIS0223", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));
    }

    @Test
    public void abortPayment() throws IOException {
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot proses payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // ABORT after
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        if (result.getPaymentResult().getStatusCode() != TransactionStatusType.OK){
            LOGGER.error(result.getPaymentResult().getMessage());
        } else {
            LOGGER.info("Provider Tx Code:" + result.getPaymentResult().getProviderTxCode());
            LOGGER.info("Auth Code:" + result.getPaymentResult().getBankAuthorizationCode());
        }
        // SIS0223 - The pre-authorisation  to be cancelled is not authorised
        assertThat("response status is ERROR - SIS0223", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.ERROR)));

    }

    @Test
    public void bigAbort() throws IOException {

        // DEFERRED first
        ProcessPaymentRequest processPaymentRequest = Fixtures.getProcessPaymentWithUniqueId(Fixtures.getPaymentRequest());
        processPaymentRequest.getPaymentRequest().setType(TransactionType.DEFERRED);
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1000));
        ProcessPaymentResponse result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);
        String txCode = result.getPaymentResult().getProviderTxCode();

        assertThat("cannot deferre payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));

        // big ABORT
        processPaymentRequest = Fixtures.getPaymentRequest();
        processPaymentRequest.getPaymentRequest().getAmountTotal().setAmount(new BigDecimal(1500));
        processPaymentRequest.getPaymentRequest().setProviderTxCode(txCode);
        processPaymentRequest.getPaymentRequest().setType(TransactionType.ABORT);
        result = this.paymentService.processPayment(this.paymentProvider, processPaymentRequest);

        assertThat("cannot abort payment", result.getPaymentResult().getStatusCode(), is(equalTo(TransactionStatusType.OK)));
    }
    
}

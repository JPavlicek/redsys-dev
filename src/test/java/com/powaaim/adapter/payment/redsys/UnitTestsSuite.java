package com.powaaim.adapter.payment.redsys;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapperFactoryTest;
import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapperTest;
import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentResponseFactoryTest;
import com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.PaymentRequestMapperTest;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequestTest;
import com.powaaim.adapter.payment.redsys.client.model.responses.RedSysResponseTest;

@RunWith(Categories.class)
@Suite.SuiteClasses({
	ProcessPaymentRequestMapperTest.class,
    PaymentRequestMapperTest.class,
    RedSysRequestTest.class,
    ProcessPaymentRequestMapperFactoryTest.class,
    ProcessPaymentResponseFactoryTest.class,
    RedSysResponseTest.class
})
public class UnitTestsSuite {}

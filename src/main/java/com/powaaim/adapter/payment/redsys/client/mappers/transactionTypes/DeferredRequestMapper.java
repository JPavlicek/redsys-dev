package com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes;

import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysTransactionType;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.request.ProcessPaymentRequest;

/**
 * Mapper for AIM DEFERRED request type
 *
 */
public class DeferredRequestMapper extends ProcessPaymentRequestMapper<RedSysRequest> {

    public DeferredRequestMapper(PaymentProvider paymentProvider){
        super(paymentProvider);
    }
    
	@Override
	protected RedSysRequest buildWithBuilder(ProcessPaymentRequest processPaymentRequest) {
		RedSysRequest req = new RedSysRequest();
		
		// both common fields and card details included
		req.setTransactionType(RedSysTransactionType.DFPreauthorization);
		mapGeneral(processPaymentRequest, req);
		mapCard(processPaymentRequest, req);
		req.computeHash(getPaymentProvider().getMerchantPassword());		
		return req;
	}
    
}

package com.powaaim.adapter.payment.redsys.client.deserializers;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.powaaim.adapter.payment.redsys.client.model.responses.RedSysResponse;
import com.powaaim.common.exception.CustomException;

/**
 * RedSysResponseDeserializer transforms reponse from RedSys gateway into RedSysResponse object
 *
 */
public class RedSysResponseDeserializer {

	
	/** transforms XML into RedSysResponse object */
	public static RedSysResponse deserialize(InputStream is) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(RedSysResponse.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			RedSysResponse response = (RedSysResponse) jaxbUnmarshaller.unmarshal(is);
			return response;
        } catch (JAXBException e) {
			throw new CustomException("Failed to decode response", e);
        }
	}
	
}

package com.powaaim.adapter.payment.redsys.client.mappers;

import java.math.BigDecimal;

import javax.ws.rs.core.MultivaluedMap;

import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.common.exception.CustomException;
import com.powaaim.common.model.bean.Money;
import com.powaaim.common.model.bean.PaymentCard;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.bean.PaymentRequest;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * Common abstract class for specific request transformers.
 * Do the common tasks mapping from ProcessPaymentRequest into POST request parameters.
 * 
 */
public abstract class ProcessPaymentRequestMapper<T extends RedSysRequest> extends com.powaaim.adapter.payment.client.mappers.ProcessPaymentRequestMapper<T> {

    public ProcessPaymentRequestMapper(PaymentProvider paymentProvider){
        super(paymentProvider);
    }

    /**
     * Maps RedSysRequest into POST request params. XML serialization is done in RedSysRequest buildXML method.
     * This method is also responsible for auth hash compute.
     * RedSys expect POST request with only one field "entrada" that contains request in XML format
     * @param request request to be transformed
     * @return POST request parameters
     */
    public MultivaluedMap<String, String> mapRequestToFormFields(RedSysRequest request) {
    	MultivaluedMap<String, String> result = new MultivaluedMapImpl();
    	result.add("entrada", request.buildXML());  	
    	return result;
    }
    
    /**
     * Maps fields that are common for every request
     * @param processPaymentRequest input ProcessPaymentRequest
     * @param req input RedSysRequest
     * @return RedSysRequest
     */
    protected RedSysRequest mapGeneral(ProcessPaymentRequest processPaymentRequest, RedSysRequest req) {
		PaymentRequest paymentRequest = processPaymentRequest.getPaymentRequest();
		PaymentProvider paymentProvider = getPaymentProvider();
		if (!paymentProvider.getProperties().containsKey("terminal")) {
			throw new CustomException("Missing terminal number in paymentProvider properties!");
		}
		
		req.setAmount(mapAmount(paymentRequest));
		req.setOrder(paymentRequest.getMerchantTxCode()); 	//TODO:what's difference between merchant and provider Tx code?
		req.setMerchantCode(paymentProvider.getMerchantId());
		req.setCurrency(Integer.toString(paymentRequest.getAmountTotal().getCurrency().getNumericCode()));
		req.setTerminal(paymentProvider.getProperties().get("terminal")); //TODO:confirm this is the right property		
		return req;
    }
    
    /**
     * Maps fields that are common for requests with credit card number
     * @param processPaymentRequest input ProcessPaymentRequest
     * @param req input RedSysRequest
     * @return RedSysRequest
     */
    protected RedSysRequest mapCard(ProcessPaymentRequest processPaymentRequest, RedSysRequest req) {
		PaymentRequest paymentRequest = processPaymentRequest.getPaymentRequest();
		PaymentCard card = paymentRequest.getPaymentCard();

		req.setPan(card.getNumber());
		req.setCvv2(card.getCvv());
		req.setExpiryDate(card.getExpirationDate().toString("yyMM") ); // order: year and month
		return req;
    }

    /**
     * Maps amount
     * @param paymentRequest
     * @return String value of money
     */
    protected String mapAmount(PaymentRequest paymentRequest) {
        String result = null;
        Money priceToCharge = paymentRequest.getAmountTotal();
        if(priceToCharge != null){
        	result = priceToCharge.getAmount().movePointRight(2).setScale(0, BigDecimal.ROUND_DOWN).toString();
        }
        return result;
    }

    /**
     * Maps currency
     * @param paymentRequest
     * @return ISO code for currency
     */
    protected String mapCurrency(PaymentRequest paymentRequest) {
        String result = null;
        Money priceToCharge = paymentRequest.getAmountTotal();
        if(priceToCharge != null){
            result = priceToCharge.getCurrency().getCurrencyCode();
        }
        return result;
    }
	
}

package com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes;

import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysTransactionType;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.request.ProcessPaymentRequest;

/**
 * Mapper for AIM REFUND and VOID request type
 *
 */
public class RefundRequestMapper extends ProcessPaymentRequestMapper<RedSysRequest> {

    public RefundRequestMapper(PaymentProvider paymentProvider){
        super(paymentProvider);
    }
        
	@Override
	protected RedSysRequest buildWithBuilder(ProcessPaymentRequest processPaymentRequest) {
		RedSysRequest req = new RedSysRequest();
		
		// include only common fileds
		req.setTransactionType(RedSysTransactionType.Refund);
		mapGeneral(processPaymentRequest, req);
		req.computeHash(getPaymentProvider().getMerchantPassword());		
		return req;
	}

}

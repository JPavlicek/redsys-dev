package com.powaaim.adapter.payment.redsys.client;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.adapter.payment.ProviderRequestFailedException;
import com.powaaim.adapter.payment.redsys.client.deserializers.RedSysResponseDeserializer;
import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapperFactory;
import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentResponseFactory;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.adapter.payment.redsys.client.model.responses.RedSysResponse;
import com.powaaim.common.exception.CustomException;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.util.HttpClient;

/**
 * AdapterClient transforms ProcessPaymentRequest into RedSys specific request,
 * contacts RedSys gateway and processRedSys XML response into RedSysResponse
 *
 */

public class PaymentProviderClient extends com.powaaim.adapter.payment.client.PaymentProviderClient<RedSysRequest, RedSysResponse> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentProviderClient.class);

    public PaymentProviderClient(PaymentProvider paymentProvider, HttpClient client) {
        super(paymentProvider, client);
    }

    @Override
    protected com.powaaim.adapter.payment.client.mappers.ProcessPaymentRequestMapperFactory initRequestMapperFactory() {
        return ProcessPaymentRequestMapperFactory.init();
    }

    @Override
    protected com.powaaim.adapter.payment.client.mappers.ProcessPaymentResponseFactory<RedSysResponse> initResponseFactory() {
        return ProcessPaymentResponseFactory.init();
    }

    @Override
    protected Class<RedSysResponse> getResponseClass() {
        return RedSysResponse.class;
    }


    /**
     * calls RedSys gateway
     * RedSysRequest structure is converted into XML and then into POST query string and sent to gateway
     * Server response is XML string that is transformed into RedSysResponse structure 
     * @param request RedSysRequest 
     * @param mapper Mapper that transforms RedSysRequest into POST quiery string
     * @return server response in RedSysResponse object
     */
    
    @Override
    public RedSysResponse postPayment(RedSysRequest request, com.powaaim.adapter.payment.client.mappers.ProcessPaymentRequestMapper<RedSysRequest> mapper) {
    	
        LOGGER.info("Posting RedSys request: {}", request);

        // the mapper only puts XML string into "entrada" field 
        ProcessPaymentRequestMapper m = (ProcessPaymentRequestMapper)mapper;
        MultivaluedMap queryParams = m.mapRequestToFormFields(request);
        
        // url is same for all request types
    	String url = getPaymentProvider().getUrl();
        InputStream responseInputStream = (InputStream)getClient().sendRequest(HttpClient.RequestType.POST, MediaType.APPLICATION_FORM_URLENCODED_TYPE, queryParams, InputStream.class, url);
        
        // response is in XML format. Deserializer transforms XML into RedSysResponse object
        return RedSysResponseDeserializer.deserialize(responseInputStream);
    }
}

package com.powaaim.adapter.payment.redsys.client.model.requests;

import java.io.StringWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.validation.constraints.NotNull;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.common.exception.CustomException;


/**
 * POJO class representing RedSys request.
 */

/*
<!ELEMENT DATOSENTRADA
(DS_Version,
DS_MERCHANT_AMOUNT,
DS_MERCHANT_CURRENCY,
DS_MERCHANT_ORDER,
DS_MERCHANT_MERCHANTCODE,
DS_MERCHANT_MERCHANTURL,
DS_MERCHANT_MERCHANTNAME ?,
DS_MERCHANT_CONSUMERLANGUAGE ?,
DS_MERCHANT_MERCHANTSIGNATURE,
DS_MERCHANT_TERMINAL,
DS_MERCHANT_TRANSACTIONTYPE,
DS_MERCHANT_MERCHANTDATA ?,
DS_MERCHANT_PAN?,
DS_MERCHANT_EXPIRYDATE ?,
DS_MERCHANT_CVV2 ?)>
*/

@XmlRootElement( name="DATOSENTRADA" )
@XmlAccessorType(XmlAccessType.FIELD)
public class RedSysRequest {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(RedSysRequest.class);

	@XmlElement(name = "DS_Version", required = true)
    protected String Version;

    @NotBlank
	@XmlElement(name = "DS_MERCHANT_AMOUNT", required = true)
	protected String Amount;
	
    @NotBlank
	@XmlElement(name = "DS_MERCHANT_CURRENCY", required = true)
	protected String Currency;
	
    @NotBlank
	@XmlElement(name = "DS_MERCHANT_ORDER", required = true)
	protected String Order;
	
    @NotBlank
	@XmlElement(name = "DS_MERCHANT_MERCHANTCODE", required = true)
	protected String MerchantCode;
	
	@XmlElement(name = "DS_MERCHANT_MERCHANTURL", required = true)
	protected String MerchantURL;
	
	@XmlElement(name = "DS_MERCHANT_MERCHANTNAME", required = false)
	protected String MerchantName;
	
	@XmlElement(name = "DS_MERCHANT_CONSUMERLANGUAGE", required = false)
	protected String ConsumerLanguage;
	
    @NotBlank
	@XmlElement(name = "DS_MERCHANT_MERCHANTSIGNATURE", required = true)
	protected String MerchantSignature;
	
    @NotBlank
	@XmlElement(name = "DS_MERCHANT_TERMINAL", required = true)
	protected String Terminal;
	
    @NotBlank
	@XmlElement(name = "DS_MERCHANT_TRANSACTIONTYPE", required = true)
	protected String TransactionType;
	
	@XmlElement(name = "DS_MERCHANT_MERCHANTDATA", required = false)
	protected String MerchantData;
	
	@XmlElement(name = "DS_MERCHANT_PAN", required = false)
	protected String Pan;
	
	@XmlElement(name = "DS_MERCHANT_EXPIRYDATE", required = false)
	protected String ExpiryDate;
	
	@XmlElement(name = "DS_MERCHANT_CVV2", required = true)
	protected String Cvv2;

	/**
	 * Computes SHA1 for RedSys request
	 * @param secret
	 * @return
	 */
	public String computeHash(String secret) {
		StringBuilder sb = new StringBuilder();
		sb.append(Amount);
		sb.append(Order);
		sb.append(MerchantCode);
		sb.append(Currency);
		if (Pan != null) sb.append(Pan);
		if (Cvv2 != null) sb.append(Cvv2);
		sb.append(TransactionType);
		sb.append(secret);
		
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("sha-1");
			byte[] digest = messageDigest.digest(sb.toString().getBytes());
			MerchantSignature = DatatypeConverter.printHexBinary(digest).toLowerCase();
			return MerchantSignature;
		} catch (NoSuchAlgorithmException e) {
			throw new CustomException("Problem computing request SHA1 hash", e);
		}
	}
	
	/*
	 * Seralize RedSysRequest into XML
	 */
	public String buildXML() {
	    JAXBContext context;
	    
		try {
			StringWriter sw = new StringWriter();
			context = JAXBContext.newInstance(RedSysRequest.class);
		    Marshaller m = context.createMarshaller();

		    if (LOGGER.isDebugEnabled()) {
			    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			    m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE); // <?xml header would break response parsing when request is returned back
			    m.marshal(this, sw);
			    LOGGER.debug(sw.toString());
			    m = context.createMarshaller();
			    sw = new StringWriter();
		    }

		    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.FALSE);
		    m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE); // <?xml header would break response parsing when request is returned back
		    m.marshal(this, sw);
		    return sw.toString();
		} catch (JAXBException e) {
			throw new CustomException("Problem in RedSys request to XML marchaling", e);
		}
	}

	public String getVersion() {
		return Version;
	}

	public void setVersion(String version) {
		Version = version;
	}

	public String getAmount() {
		return Amount;
	}

	public void setAmount(String amount) {
		Amount = amount;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public String getOrder() {
		return Order;
	}

	public void setOrder(String order) {
		Order = order;
	}

	public String getMerchantCode() {
		return MerchantCode;
	}

	public void setMerchantCode(String merchantCode) {
		MerchantCode = merchantCode;
	}

	public String getMerchantURL() {
		return MerchantURL;
	}

	public void setMerchantURL(String merchantURL) {
		MerchantURL = merchantURL;
	}

	public String getMerchantName() {
		return MerchantName;
	}

	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}

	public String getConsumerLanguage() {
		return ConsumerLanguage;
	}

	public void setConsumerLanguage(String consumerLanguage) {
		ConsumerLanguage = consumerLanguage;
	}

	public String getMerchantSignature() {
		return MerchantSignature;
	}

	public void setMerchantSignature(String merchantSignature) {
		MerchantSignature = merchantSignature;
	}

	public String getTerminal() {
		return Terminal;
	}

	public void setTerminal(String terminal) {
		Terminal = terminal;
	}

	public String getTransactionType() {
		return TransactionType;
	}

	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}

	public String getMerchantData() {
		return MerchantData;
	}

	public void setMerchantData(String merchantData) {
		MerchantData = merchantData;
	}

	public String getPan() {
		return Pan;
	}

	public void setPan(String pan) {
		Pan = pan;
	}

	public String getExpiryDate() {
		return ExpiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		ExpiryDate = expiryDate;
	}

	public String getCvv2() {
		return Cvv2;
	}

	public void setCvv2(String cvv2) {
		Cvv2 = cvv2;
	}
	


	
}

package com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes;

import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysTransactionType;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.request.ProcessPaymentRequest;

/**
 * Mapper for AIM ABORT request type
 *
 */
public class AbortRequestMapper extends ProcessPaymentRequestMapper<RedSysRequest> {

    public AbortRequestMapper(PaymentProvider paymentProvider){
        super(paymentProvider);
    }
        
	@Override
	protected RedSysRequest buildWithBuilder(ProcessPaymentRequest processPaymentRequest) {
		RedSysRequest req = new RedSysRequest();

		// only common fields, no card info
		req.setTransactionType(RedSysTransactionType.DFPreauthorizationCancel);
		mapGeneral(processPaymentRequest, req);
		req.computeHash(getPaymentProvider().getMerchantPassword());		
		return req;
	}

}

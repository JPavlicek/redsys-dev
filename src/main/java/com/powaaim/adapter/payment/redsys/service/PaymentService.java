package com.powaaim.adapter.payment.redsys.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import com.powaaim.adapter.payment.redsys.client.PaymentProviderClient;
import com.powaaim.adapter.payment.service.AbstractPaymentService;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;
import com.powaaim.common.util.HttpClient;

public final class PaymentService  extends AbstractPaymentService {

    public PaymentService() {}

    
    /**
     * Generates new transaction ID that complies to mask DDDDAAAAAAAA
     * where D = digit, A = alphanumeric
     *
     * First 4 characters are current year and month, mask YYMM.
     * Next 8 characters are first 8 characters from random generated UUID.
     * @return String
     */
    @Override
	protected String generateUUID() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMM");
		StringBuilder sb = new StringBuilder();
		sb.append(sdf.format(new Date()));
		sb.append(UUID.randomUUID().toString().substring(0, 8));
		return sb.toString();
	}



	public ProcessPaymentResponse process(final ProcessPaymentRequest processPaymentRequest, final PaymentProvider paymentProvider) {
        PaymentProviderClient paymentProviderClient = initClient(paymentProvider, getClient());
        return paymentProviderClient.processPayment(processPaymentRequest);
    }

    protected PaymentProviderClient initClient(final PaymentProvider paymentProvider,@SuppressWarnings("rawtypes") final HttpClient client){
    	return new PaymentProviderClient(paymentProvider, client);
    }
}

package com.powaaim.adapter.payment.redsys.client.model.requests;

/**
 * Transaction type constants. See RedSys documentation for details.
 *
 */
public class RedSysTransactionType {

	public static final String StandardPayment				= "0";

	public static final String Preauthorization 			= "1";
	
	public static final String PreauthorizationConfirm 		= "2";
	
	public static final String Refund 						= "3";
	
	public static final String Authentication 				= "7";
	
	public static final String AuthenticationConfirm 		= "8";
	
	public static final String AuthenticationCancel 		= "9";
	
	public static final String UnauthPayment 				= "A";
	
	public static final String DFPreauthorization 			= "O";
	
	public static final String DFPreauthorizationConfirm 	= "P";
	
	public static final String DFPreauthorizationCancel		= "Q";

	public static final String InitialFileCard				= "L";

	public static final String SuccessiveFileCards			= "M";
	
}

package com.powaaim.adapter.payment.redsys.client.model.responses;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;

/**
 * POJO class representing RedSys response.
 */

/*

ok response example:

<?xml version='1.0' encoding="ISO-8859-1" ?>
<RETORNOXML>
  <CODIGO>0</CODIGO>
  <OPERACION>
    <Ds_Amount>1000</Ds_Amount>
    <Ds_Currency>978</Ds_Currency>
    <Ds_Order>150824POWA01</Ds_Order>
    <Ds_Signature>08A63C62850FA8F63A7BAB278AFFACB05D0ECEE1</Ds_Signature>
    <Ds_MerchantCode>327234688</Ds_MerchantCode>
    <Ds_Terminal>2</Ds_Terminal>
    <Ds_Response>0000</Ds_Response>
    <Ds_AuthorisationCode>426299</Ds_AuthorisationCode>
    <Ds_TransactionType>A</Ds_TransactionType>
    <Ds_SecurePayment>0</Ds_SecurePayment>
    <Ds_Language>1</Ds_Language>
    <Ds_MerchantData></Ds_MerchantData>
    <Ds_Card_Country>724</Ds_Card_Country>
  </OPERACION>
</RETORNOXML>


error response example:

<?xml version='1.0' encoding="ISO-8859-1" ?>
<RETORNOXML>
  <CODIGO>SIS0252</CODIGO>
  <RECIBIDO>
    <DATOSENTRADA>
      <DS_MERCHANT_AMOUNT>1000</DS_MERCHANT_AMOUNT>
      <DS_MERCHANT_ORDER>150814POWA06</DS_MERCHANT_ORDER>
      <DS_MERCHANT_MERCHANTCODE>297395907</DS_MERCHANT_MERCHANTCODE>
      <DS_MERCHANT_CURRENCY>978</DS_MERCHANT_CURRENCY>
      <DS_MERCHANT_PAN>4548812049400004</DS_MERCHANT_PAN>
      <DS_MERCHANT_CVV2>123</DS_MERCHANT_CVV2>
      <DS_MERCHANT_TRANSACTIONTYPE>A</DS_MERCHANT_TRANSACTIONTYPE>
      <DS_MERCHANT_TERMINAL>2</DS_MERCHANT_TERMINAL>
      <DS_MERCHANT_EXPIRYDATE>2012</DS_MERCHANT_EXPIRYDATE>
      <DS_MERCHANT_MERCHANTSIGNATURE>56a91e68ee25d6f18fcaec1d0c32f247689823e3</DS_MERCHANT_MERCHANTSIGNATURE>
    </DATOSENTRADA>
  </RECIBIDO>
</RETORNOXML>
 */


@XmlRootElement( name="RETORNOXML" )
@XmlAccessorType(XmlAccessType.FIELD)
public class RedSysResponse {
	
	@XmlElement(name = "CODIGO")
	String codigo;
	
	@XmlElement(name="RECIBIDO")
	Recibido recibido;

	@XmlElement(name="OPERACION")
	Operacion operacion;

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Recibido getRecibido() {
		return recibido;
	}

	public void setRecibido(Recibido recibido) {
		this.recibido = recibido;
	}

	public Operacion getOperacion() {
		return operacion;
	}

	public void setOperacion(Operacion operacion) {
		this.operacion = operacion;
	}

	
	
	
	// how to deserialize nested XML
	// http://www.coderanch.com/t/595215/XML/Unmarshalling-nested-child-elements-jaxb
		


	@XmlRootElement(name="RECIBIDO")
	public static class Recibido {
		
		// in case of error, holds original request
		@XmlElement(name="DATOSENTRADA")
		RedSysRequest datosentrada;
		
		// contains the response in case of no problem
		public RedSysRequest getDatosentrada () {
			return datosentrada;
		}
	}
	
	// real response data
	@XmlRootElement( name="OPERACION" )
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class Operacion {

		@XmlElement(name = "Ds_Amount")
		String amount;
		
		@XmlElement(name = "Ds_Currency")
		String currency;
		
		@XmlElement(name = "Ds_Order")
		String order;
		
		@XmlElement(name = "Ds_Signature")
		String merchantSignature;
		
		@XmlElement(name = "Ds_MerchantCode")
		String merchantCode;
		
		@XmlElement(name = "Ds_Terminal")
		String terminal;
		
		@XmlElement(name = "Ds_Response")
		String response;
		
		@XmlElement(name = "Ds_AuthorisationCode")
		String authorisationCode;
		
		@XmlElement(name = "Ds_TransactionType")
		String transactionType;

		@XmlElement(name = "Ds_SecurePayment")
		String securePayment;
		
		@XmlElement(name = "Ds_Language")
		String language;
				
		@XmlElement(name = "Ds_MerchantData")
		String merchantData;
		
		@XmlElement(name = "Ds_Card_Country")
		String cardCountry;

		public String getAmount() {
			return amount;
		}

		public String getCurrency() {
			return currency;
		}

		public String getOrder() {
			return order;
		}

		public String getMerchantSignature() {
			return merchantSignature;
		}

		public String getMerchantCode() {
			return merchantCode;
		}

		public String getTerminal() {
			return terminal;
		}

		public String getResponse() {
			return response;
		}

		public String getAuthorisationCode() {
			return authorisationCode;
		}

		public String getTransactionType() {
			return transactionType;
		}

		public String getSecurePayment() {
			return securePayment;
		}

		public String getLanguage() {
			return language;
		}

		public String getMerchantData() {
			return merchantData;
		}

		public String getCardCountry() {
			return cardCountry;
		}
		
		
		
	}	
}

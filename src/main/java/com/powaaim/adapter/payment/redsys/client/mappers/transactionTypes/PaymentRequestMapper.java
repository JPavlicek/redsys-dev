package com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes;

import com.powaaim.adapter.payment.redsys.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysTransactionType;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.request.ProcessPaymentRequest;

/**
 * Mapper for AIM PAYMENT request type
 *
 */
public class PaymentRequestMapper extends ProcessPaymentRequestMapper<RedSysRequest> {

    public PaymentRequestMapper(PaymentProvider paymentProvider){
        super(paymentProvider);
    }
        
	@Override
	protected RedSysRequest buildWithBuilder(ProcessPaymentRequest processPaymentRequest) {
		RedSysRequest req = new RedSysRequest();
		
		// include common fields and card details
		req.setTransactionType(RedSysTransactionType.UnauthPayment);
		mapGeneral(processPaymentRequest, req);
		mapCard(processPaymentRequest, req);
		req.computeHash(getPaymentProvider().getMerchantPassword());		
		return req;
	}

}

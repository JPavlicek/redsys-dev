package com.powaaim.adapter.payment.redsys.client.mappers;

import com.powaaim.adapter.payment.redsys.client.model.responses.RedSysResponse;
import com.powaaim.adapter.payment.redsys.client.model.responses.SISErrors;
import com.powaaim.common.model.bean.PaymentResult;
import com.powaaim.common.model.constant.PaymentProviderType;
import com.powaaim.common.model.constant.TransactionStatusType;
import com.powaaim.common.model.request.ProcessPaymentRequest;
import com.powaaim.common.model.response.ProcessPaymentResponse;


/**
 * Maps RedSys response into ProcessPaymentResponse
 * There is one path for processing of all type of requests.
 *
 */
public class ProcessPaymentResponseFactory extends com.powaaim.adapter.payment.client.mappers.ProcessPaymentResponseFactory<RedSysResponse>{

    public static com.powaaim.adapter.payment.client.mappers.ProcessPaymentResponseFactory<RedSysResponse> init(){
        return new ProcessPaymentResponseFactory();
    }
    
    @Override
    protected Builder initBuilder(ProcessPaymentRequest processPaymentRequest) {
        return new Builder(processPaymentRequest);
    }



    protected static final class Builder extends AbstractResponseBuilder<RedSysResponse, Builder>{

        protected Builder(ProcessPaymentRequest processPaymentRequest) {
            super(processPaymentRequest);
        }

        protected Builder(ProcessPaymentResponse response) {
            super(response);
        }

        @Override
        public PaymentProviderType getPaymentProvider() {
            return PaymentProviderType.SAGEPAY; //TODO: change to .REDSYS once defined in API
        }

        /**
         * this code actually does the mapping
         * @param pspResponse RedSys response
         */
        public Builder applyPSPResponse(RedSysResponse pspResponse) {

            ProcessPaymentResponse response = getProcessPaymentResponse();
            PaymentResult paymentResult = response.getPaymentResult();
            
            if (paymentResult != null) {
            	
            	String codigo = pspResponse.getCodigo();
            	if ("0".equals(codigo)) {
            		// no error
            		paymentResult.setStatusCode(TransactionStatusType.OK);
            		RedSysResponse.Operacion op = pspResponse.getOperacion();
                    paymentResult.setAuthorizationCode(op.getAuthorisationCode());
                    paymentResult.setProviderTxCode(op.getOrder());
            	} else {
            		// error
            		// in this case map error code and message
            		SISErrors error = SISErrors.valueOf(codigo);
            		paymentResult.setBankDeclineCode(codigo);
            		if (!error.equals(SISErrors.SIS9999)) {
                		paymentResult.setStatusCode(error.getTransactionStatusType());
                		paymentResult.setMessage(error.getErrorMessage());            			
            		} else {
                		paymentResult.setStatusCode(TransactionStatusType.ERROR);
                		paymentResult.setMessage(codigo);            			
            		}
            	}
            }            
            return this;
        }
    }	
}

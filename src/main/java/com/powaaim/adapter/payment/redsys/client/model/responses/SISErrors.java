package com.powaaim.adapter.payment.redsys.client.model.responses;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.powaaim.common.model.constant.TransactionStatusType;


public enum SISErrors {
	SIS0007(TransactionStatusType.MALFORMED, "Error disassembling input XML"),
	SIS0008(TransactionStatusType.MALFORMED, "Field missing"),
	SIS0009(TransactionStatusType.MALFORMED, "Format error"),
	SIS0010(TransactionStatusType.MALFORMED, "Field missing"),
	SIS0011(TransactionStatusType.MALFORMED, "Format error"),
	SIS0014(TransactionStatusType.MALFORMED, "Format error"),
	SIS0015(TransactionStatusType.MALFORMED, "Field missing"),
	SIS0016(TransactionStatusType.MALFORMED, "Format error"),
	SIS0018(TransactionStatusType.MALFORMED, "Field missing"),
	SIS0019(TransactionStatusType.MALFORMED, "Format error"),
	SIS0020(TransactionStatusType.MALFORMED, "Field missing"),
	SIS0021(TransactionStatusType.MALFORMED, "Field empty"),
	SIS0022(TransactionStatusType.MALFORMED, "Format error"),
	SIS0023(TransactionStatusType.MALFORMED, "Unknown value"),
	SIS0024(TransactionStatusType.MALFORMED, "Value exceeds 3 positions"),
	SIS0025(TransactionStatusType.MALFORMED, "Format error"),
	SIS0026(TransactionStatusType.ERROR, "Error Merchant inexistent / Terminal sent"),
	SIS0027(TransactionStatusType.ERROR, "Error currency does not match that assigned for that Terminal."),
	SIS0028(TransactionStatusType.ERROR, "Error Merchant/Terminal is de-registered"),
	SIS0030(TransactionStatusType.ERROR, "In card payment a type of operation has arrived which is not payment nor pre-authorisation"),
	SIS0031(TransactionStatusType.ERROR, "Method of payment not defined"),
	SIS0034(TransactionStatusType.ERROR, "Error accessing database"),
	SIS0038(TransactionStatusType.ERROR, "Error in JAVA"),
	SIS0040(TransactionStatusType.ERROR, "The merchant / Terminal has not assigned method of payment"),
	SIS0041(TransactionStatusType.ERROR, "Error calculating HASH algorithm"),
	SIS0042(TransactionStatusType.ERROR, "Error calculating HASH algorithm"),
	SIS0043(TransactionStatusType.ERROR, "Error making online notification"),
	SIS0046(TransactionStatusType.REJECTED, "Card Pin not registered"),
	SIS0051(TransactionStatusType.REJECTED, "Repeated order number"),
	SIS0054(TransactionStatusType.REJECTED, "No operation to make refund"),
	SIS0055(TransactionStatusType.REJECTED, "Operation to be refunded is not valid"),
	SIS0056(TransactionStatusType.REJECTED, "Operation to be refunded is not authorised"),
	SIS0057(TransactionStatusType.REJECTED, "Amount to be refunded exceeds limit"),
	SIS0058(TransactionStatusType.REJECTED, "Inconsistent data in validation of confirmation"),
	SIS0059(TransactionStatusType.REJECTED, "Error, operation for confirmation does not exist"),
	SIS0060(TransactionStatusType.REJECTED, "Confirmation for this pre-authorisation already exists"),
	SIS0061(TransactionStatusType.REJECTED, "The pre-authorisation  to be confirmed is not authorised"),
	SIS0062(TransactionStatusType.REJECTED, "Amount to be confirmed exceeds limit"),
	SIS0063(TransactionStatusType.REJECTED, "Error in card number"),
	SIS0064(TransactionStatusType.REJECTED, "Error in card number"),
	SIS0065(TransactionStatusType.REJECTED, "Error in card number"),
	SIS0066(TransactionStatusType.REJECTED, "Error in card expiry date"),
	SIS0067(TransactionStatusType.REJECTED, "Error in card expiry date"),
	SIS0068(TransactionStatusType.REJECTED, "Error in card expiry date"),
	SIS0069(TransactionStatusType.REJECTED, "Error in card expiry date"),
	SIS0070(TransactionStatusType.REJECTED, "Error in card expiry date"),
	SIS0071(TransactionStatusType.REJECTED, "Expired card"),
	SIS0072(TransactionStatusType.REJECTED, "Operation cannot be cancelled"),
	SIS0074(TransactionStatusType.ERROR, "Field missing"),
	SIS0075(TransactionStatusType.ERROR, "Value has fewer than 4 positions or more than 12"),
	SIS0076(TransactionStatusType.ERROR, "Value is not numerical"),
	SIS0078(TransactionStatusType.ERROR, "Unknown value"),
	SIS0079(TransactionStatusType.ERROR, "Error when make payment by card"),
	SIS0081(TransactionStatusType.ERROR, "The session is new, you have lost the data stored"),
	SIS0089(TransactionStatusType.ERROR, "Ds_Merchant_ExpiryDate value not set in 4 positions"),
	SIS0092(TransactionStatusType.ERROR, "Ds_Merchant_ExpiryDate value is null"),
	SIS0093(TransactionStatusType.ERROR, "Card not found within table of ranges"),
	SIS0094(TransactionStatusType.ERROR, "Card not authenticated as 3D Secure"),
	SIS0112(TransactionStatusType.ERROR, "Value not allowed"),
	SIS0114(TransactionStatusType.ERROR, "A GET has been called instead of a POST"),
	SIS0115(TransactionStatusType.ERROR, "No operation to make instalment payment"),
	SIS0116(TransactionStatusType.ERROR, "Operation for instalment payment is not valid."),
	SIS0117(TransactionStatusType.ERROR, "Operation for instalment payment is not authorised."),
	SIS0119(TransactionStatusType.ERROR, "Transaction rejected. Contact with your Bank."),
	SIS0132(TransactionStatusType.ERROR, "The Confirmation of Authorisation date cannot exceed pre-authorisation date by more than 7 days"),
	SIS0133(TransactionStatusType.ERROR, "The confirmation of Authentication date cannot exceed prior authentication by more than 45 days"),
	SIS0139(TransactionStatusType.ERROR, "Initial recurrent payment is duplicated"),
	SIS0142(TransactionStatusType.ERROR, "Time exceeded for payment"),
	SIS0198(TransactionStatusType.ERROR, "Amount exceeds limit allowed for merchant"),
	SIS0199(TransactionStatusType.ERROR, "The number of operations exceeds limit allowed for merchant"),
	SIS0200(TransactionStatusType.ERROR, "Amount accumulated exceeds limit allowed for merchant"),
	SIS0214(TransactionStatusType.ERROR, "Merchant does not accept refunds"),
	SIS0216(TransactionStatusType.ERROR, "The CVV2 has more than three positions"),
	SIS0217(TransactionStatusType.ERROR, "Format error in CVV2"),
	SIS0218(TransactionStatusType.ERROR, "Operations input does not allow secure payments"),
	SIS0219(TransactionStatusType.ERROR, "The number of card operations exceeds limit allowed for merchant"),
	SIS0220(TransactionStatusType.ERROR, "Accumulated amount of card exceeds limit allowed for merchant"),
	SIS0221(TransactionStatusType.ERROR, "Error. The CVV2 is required:"),
	SIS0222(TransactionStatusType.ERROR, "Cancellation for this pre-authorisation already exists"),
	SIS0223(TransactionStatusType.ERROR, "The pre-authorisation  to be cancelled is not authorised"),
	SIS0224(TransactionStatusType.ERROR, "Merchant does not allow cancellations due to lack of extended signature"),
	SIS0225(TransactionStatusType.ERROR, "No operation to make cancellation"),
	SIS0226(TransactionStatusType.ERROR, "Inconsistent data in validation of a cancellation"),
	SIS0227(TransactionStatusType.ERROR, "Invalid value"),
	SIS0229(TransactionStatusType.ERROR, "No deferred payment code requested"),
	SIS0252(TransactionStatusType.ERROR, "Merchant does not allow card to be sent"),
	SIS0253(TransactionStatusType.ERROR, "Card does not comply with check-digit"),
	SIS0254(TransactionStatusType.ERROR, "The number of operations per IP exceeds limit allowed for merchant"),
	SIS0255(TransactionStatusType.ERROR, "Amount accumulated per IP exceeds limit allowed for merchant"),
	SIS0256(TransactionStatusType.ERROR, "Merchant cannot perform pre-authorisations."),
	SIS0257(TransactionStatusType.ERROR, "Card does not allow pre-authorisations"),
	SIS0258(TransactionStatusType.ERROR, "Inconsistent confirmation data"),
	SIS0261(TransactionStatusType.ERROR, "Operation exceeds an operating limit defined by Banco Sabadell"),
	SIS0270(TransactionStatusType.ERROR, "Type of operation not activated for this merchant"),
	SIS0274(TransactionStatusType.ERROR, "Type of operation unknown or not allowed for this input to the Virtual POS."),
	SIS0281(TransactionStatusType.ERROR, "Operation exceeds an operating limit defined by Banco Sabadell"),
	SIS0296(TransactionStatusType.ERROR, "Error validating initial operation data Card on File (Subscriptions P./Express P)."),
	SIS0297(TransactionStatusType.ERROR, "Maximum number of operations exceeded (99 oper.or 1 year) for successive transactions in Card on File (Subscriptions P. /Express P.).A new Initial File Card operation is necessary to start the cycle."),
	SIS0298(TransactionStatusType.ERROR, "The merchant does not allow Card on File operations"),
	SIS0319(TransactionStatusType.ERROR, "The merchant does not belong to the group specified in Ds_Merchant_Group"),
	SIS0321(TransactionStatusType.ERROR, "The reference indicated in Ds_Merchant_Identifier is not associated with the merchant"),
	SIS0322(TransactionStatusType.ERROR, "Format error in Ds_Merchant_Group"),
	SIS0325(TransactionStatusType.ERROR, "It was requested not to show screens but no card reference has been sent"),
	SIS9999(TransactionStatusType.ERROR, "Unknown error");
	
    private static final Logger LOGGER = LoggerFactory.getLogger(SISErrors.class);

    private TransactionStatusType transactionStatusType;
    private String message;
	
	SISErrors(TransactionStatusType transactionStatusType, String message) {
		this.transactionStatusType = transactionStatusType;
		this.message = message;		
	}
	
	public TransactionStatusType getTransactionStatusType() {
		return transactionStatusType;
	}

	public String getMessage() {
		return message;
	}

    public static SISErrors fromValue(String id) {

        for (SISErrors code : SISErrors.values()) {
            if (code.name().equals(id)) {
                return code;
            }
        }

        LOGGER.warn("Couldn't find enum for RedSysErrorCodes: " + id + "! Will be considered as UNKNOWN_ERROR!");
        return SIS9999;
    }

	public String getErrorMessage() {
		SISErrors e = this;
		if (e.equals(SIS9999)) return name();
		StringBuilder sb = new StringBuilder();
		sb.append(name());
		sb.append(" - ");
		sb.append(e.getMessage());
		return sb.toString();
	}

}

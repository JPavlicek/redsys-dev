package com.powaaim.adapter.payment.redsys.client.mappers;

import com.powaaim.adapter.payment.FunctionalityNotSupportedException;
import com.powaaim.adapter.payment.client.mappers.ProcessPaymentRequestMapper;
import com.powaaim.adapter.payment.redsys.client.model.requests.RedSysRequest;
import com.powaaim.common.model.bean.PaymentProvider;
import com.powaaim.common.model.constant.TransactionType;

/**
 * Assigns proper mapper according to transaction type
 * That mapper will transform ProcessPaymentRequest into POST request that goes into RedSys gateway
 * VOID and REFUND are mapped to same RedSys transaction "Refund"
 */
public class ProcessPaymentRequestMapperFactory extends com.powaaim.adapter.payment.client.mappers.ProcessPaymentRequestMapperFactory<RedSysRequest, ProcessPaymentRequestMapper<RedSysRequest>>{


	
    public static ProcessPaymentRequestMapperFactory init(){
        return new ProcessPaymentRequestMapperFactory();
    }

    
    protected ProcessPaymentRequestMapper<RedSysRequest> getMapper(TransactionType type, PaymentProvider paymentProvider){
        if(type != null){
            switch (type){
                case PAYMENT:
                    return new com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.PaymentRequestMapper(paymentProvider);
	            case DEFERRED:
                    return new com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.DeferredRequestMapper(paymentProvider);
	            case CAPTURE:
                    return new com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.CaptureRequestMapper(paymentProvider);
	            case ABORT:
                    return new com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.AbortRequestMapper(paymentProvider);
	            case VOID:
	            	return new com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.RefundRequestMapper(paymentProvider);
	            case REFUND:
                    return new com.powaaim.adapter.payment.redsys.client.mappers.transactionTypes.RefundRequestMapper(paymentProvider);
                default:
                    throw new FunctionalityNotSupportedException("Unsupported transaction type " + type);

            }
        } else {
            throw new FunctionalityNotSupportedException("Unsupported transaction type " + type);
        }
    }
	
}
